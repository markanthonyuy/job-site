import axios from 'axios';
import { GET_ITEMS, ADD_ITEM, DELETE_ITEM, EDIT_ITEM, ITEMS_LOADING } from './types';

export const getItems = () => dispatch => {
  dispatch(setItemsLoading());
  axios.get('/api/items').then(res => dispatch({
    type: GET_ITEMS,
    payload: res.data
  }));
}

export const deleteItem = id => dispatch => {
  axios.delete(`/api/items/${id}`).then(res => dispatch({
    type: DELETE_ITEM,
    payload: id
  }));
}

export const addItem = item => dispatch => {
  dispatch(setItemsLoading());
  axios.post('/api/items', item).then(res => dispatch({
    type: ADD_ITEM,
    payload: res.data
  }));
}

export const editItem = (id, item) => dispatch => {
  console.log(item);
  axios.put(`/api/items/${id}`, item).then(res => dispatch({
    type: EDIT_ITEM,
    payload: item,
    id: id
  }));
}

export const setItemsLoading = () => {
  return {
    type: ITEMS_LOADING,
  };
}