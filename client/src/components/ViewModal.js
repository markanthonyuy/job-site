import React, { Component } from 'react';
import {
  Modal,
  ModalHeader,
  ModalBody,
  Button
} from 'reactstrap';
import TimeAgo from 'react-timeago';
import 'font-awesome/css/font-awesome.min.css';

class ViewModal extends Component {

  state = {
    modal: false,
  }

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }

  render() {

    return(
      <span>
        <Button
          className="btn float-right"
          color="info"
          style={{ marginRight: '5px'}}
          size="sm"
          onClick={this.toggle}
        ><i className="fa fa-eye" /></Button>

        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          size="lg"
        >
          <ModalHeader toggle={this.toggle}>{this.props.name} @ {this.props.company}</ModalHeader>
          <ModalBody>
            <p><b>Posted</b>: <TimeAgo date={this.props.date} /><br /><b>Type</b>: {this.props.type}</p>
            {this.props.description &&
              <div>
                <h5><b>Description:</b></h5>
                <p dangerouslySetInnerHTML={{__html: this.props.description}} />
              </div>
            }
            {this.props.requirements &&
              <div>
                <h5><b>Requirements:</b></h5>
                <p dangerouslySetInnerHTML={{__html: this.props.requirements}} />
              </div>
            }
            {this.props.perks &&
              <div>
                <h5><b>Perks:</b></h5>
                <p dangerouslySetInnerHTML={{__html: this.props.perks}} />
              </div>
            }
            {this.props.apply &&
              <div>
                <h5><b>How to Apply:</b></h5>
                <p dangerouslySetInnerHTML={{__html: this.props.apply}} />
              </div>
            }
            </ModalBody>
        </Modal>
      </span>
    )
  }
}

export default ViewModal;