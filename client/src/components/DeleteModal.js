import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Alert
} from 'reactstrap';
import { connect } from 'react-redux';
import { deleteItem } from '../actions/jobActions';
import TimeAgo from 'react-timeago';

class DeleteModal extends Component {
  state = {
    modal: false,
  }

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }

  onSubmit = e => {
    e.preventDefault();

    this.toggle();
  }

  onDeleteClick = id => {
    this.props.deleteItem(id);
  }

  render() {
    return(
      <span>
        <Button
          className="remove-btn float-right"
          color="danger"
          size="sm"
          onClick={this.toggle}
          ><i className="fa fa-trash" /></Button>

        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
        >
          <ModalHeader toggle={this.toggle}>Delete Job</ModalHeader>
          <ModalBody>
            <p>Are you sure you want to delete this Job?</p>
            <Alert color="info">
              <b>Name</b>: {this.props.name} <br />
              <b>Company</b>: {this.props.company} <br />
              <b>Type</b>: {this.props.type} <br />
              <b>Posted</b>: <TimeAgo date={this.props.date} />
            </Alert>
            
          </ModalBody>
          <ModalFooter> 
            <Button
              color="link"
              onClick={this.toggle}
            >Cancel</Button>
            <Button
              color="danger"
              onClick={this.onDeleteClick.bind(this, this.props.id)}
            >Yes</Button>
          </ModalFooter> 
        </Modal>
      </span>
    )
  }
}

const mapStateToProps = state => ({
  item: state.item
});

export default connect(mapStateToProps, { deleteItem })(DeleteModal);