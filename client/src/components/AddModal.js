import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  CustomInput
} from 'reactstrap';
import { connect } from 'react-redux';
import { addItem } from '../actions/jobActions';
import 'font-awesome/css/font-awesome.min.css';

class AddModal extends Component {
  state = {
    modal: false,
    name: '',
    description: '',
    type: 'Full-time',
    company: '',
    requirements: '',
    perks: '',
    apply: '',
  }

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value })
  }
  onSubmit = e => {
    e.preventDefault();

    const newItem = {
      name: this.state.name,
      description: this.state.description,
      type: this.state.type,
      company: this.state.company,
      requirements: this.state.requirements,
      perks: this.state.perks,
      apply: this.state.apply,
    }

    this.props.addItem(newItem);

    this.toggle();
  }

  render() {

    return(
      <div>
        <Button
          color="dark"
          style={{ marginBottom: '2rem'}}
          onClick={this.toggle}
        ><i className="fa fa-plus" /> Add Job</Button>

        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          size="lg"
        >
          <ModalHeader toggle={this.toggle}>Add Job</ModalHeader>
          <ModalBody>
            <Form onSubmit={this.onSubmit}>
              <FormGroup>
                <Label for="title">Title</Label>
                <Input 
                  type="text"
                  name="name"
                  id="title"
                  placeholder="Add Title"
                  onChange={this.onChange}
                  required
                />
              </FormGroup>
              <FormGroup>
                <Label>Type</Label>
                <div>
                  <CustomInput type="radio" name="type" id="exampleCustomInline1" label="Full-time" inline checked={this.state.type === 'Full-time'} value="Full-time" onChange={this.onChange} />
                  <CustomInput type="radio" name="type" id="exampleCustomInline2" label="Part-time" inline checked={this.state.type === 'Part-time'} value="Part-time" onChange={this.onChange} />
                  <CustomInput type="radio" name="type" id="exampleCustomInline3" label="Contract" inline checked={this.state.type === 'Contract'} value="Contract" onChange={this.onChange} />
                  <CustomInput type="radio" name="type" id="exampleCustomInline4" label="Freelance" inline checked={this.state.type === 'Freelance'} value="Freelance" onChange={this.onChange} />
                </div>
              </FormGroup>
              <FormGroup>
                <Label for="company">Company</Label>
                <Input 
                  type="text"
                  name="company"
                  id="company"
                  placeholder="Add Company"
                  onChange={this.onChange}
                  required
                />
              </FormGroup>
              <FormGroup>
                <Label for="description">Description</Label>
                <Input 
                  type="textarea"
                  name="description"
                  id="description"
                  placeholder="Add Descripiton"
                  onChange={this.onChange}
                  style={{ height: '200px' }}
                  required
                />
              </FormGroup>
              <FormGroup>
                <Label for="requirements">Requirements</Label>
                <Input 
                  type="textarea"
                  name="requirements"
                  id="requirements"
                  placeholder="Requirements"
                  onChange={this.onChange}
                  style={{ height: '200px' }}
                />
              </FormGroup>
              <FormGroup>
                <Label for="perks">Perks</Label>
                <Input 
                  type="textarea"
                  name="perks"
                  id="perks"
                  placeholder="Perks"
                  onChange={this.onChange}
                  style={{ height: '200px' }}
                />
              </FormGroup>
              <FormGroup>
                <Label for="apply">How to Apply</Label>
                <Input 
                  type="textarea"
                  name="apply"
                  id="apply"
                  placeholder="How to Apply"
                  onChange={this.onChange}
                  style={{ height: '100px' }}
                  required
                />
              </FormGroup>
              <Button
                color="dark"
                style={{ marginTop: '2rem' }}
                block
              >Add Item</Button>
            </Form>
          </ModalBody>
        </Modal>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  item: state.item
});

export default connect(mapStateToProps, { addItem })(AddModal);