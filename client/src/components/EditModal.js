import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  CustomInput
} from 'reactstrap';
import { connect } from 'react-redux';
import { editItem } from '../actions/jobActions';
import 'font-awesome/css/font-awesome.min.css';

class EditModal extends Component {

  state = {
    modal: false,
    name: this.props.name,
    description: this.props.description,
    type: this.props.type,
    company: this.props.company,
    requirements: this.props.requirements,
    perks: this.props.perks,
    apply: this.props.apply,
  }

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value })
  }

  onSubmit = e => {
    e.preventDefault();

    const item = {
      name: this.state.name,
      description: this.state.description,
      type: this.state.type,
      company: this.state.company,
      requirements: this.state.requirements,
      perks: this.state.perks,
      apply: this.state.apply,
    }

    this.props.editItem(this.props.id, item);

    this.toggle();
  }

  render() {

    return(
      <span>
        <Button
          className="btn float-right"
          color="warning"
          size="sm"
          style={{'marginRight': '5px'}}
          onClick={this.toggle}
        ><i className="fa fa-edit" />
        </Button>

        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          size="lg"
        >
          <ModalHeader toggle={this.toggle}>Edit Job</ModalHeader>
          <ModalBody>
            <Form onSubmit={this.onSubmit}>
              <FormGroup>
                <Label for="title">Title</Label>
                <Input 
                  type="text"
                  name="name"
                  id="title"
                  placeholder="Title"
                  onChange={this.onChange}
                  value={this.state.name}
                  required
                />
              </FormGroup>
              <FormGroup>
                <Label>Type</Label>
                <div>
                  <CustomInput type="radio" name="type" id="exampleCustomInline1" label="Full-time" inline checked={this.state.type === 'Full-time'} value="Full-time" onChange={this.onChange} />
                  <CustomInput type="radio" name="type" id="exampleCustomInline2" label="Part-time" inline checked={this.state.type === 'Part-time'} value="Part-time" onChange={this.onChange} />
                  <CustomInput type="radio" name="type" id="exampleCustomInline3" label="Contract" inline checked={this.state.type === 'Contract'} value="Contract" onChange={this.onChange} />
                  <CustomInput type="radio" name="type" id="exampleCustomInline4" label="Freelance" inline checked={this.state.type === 'Freelance'} value="Freelance" onChange={this.onChange} />
                </div>
              </FormGroup>
              <FormGroup>
                <Label for="company">Company</Label>
                <Input 
                  type="text"
                  name="company"
                  id="company"
                  placeholder="Company"
                  onChange={this.onChange}
                  value={this.state.company}
                  required
                />
              </FormGroup>
              <FormGroup>
                <Label for="description">Description</Label>
                <Input 
                  type="textarea"
                  name="description"
                  id="description"
                  placeholder="Descripiton"
                  onChange={this.onChange}
                  style={{ height: '200px' }}
                  value={this.state.description}
                  required
                />
              </FormGroup>
              <FormGroup>
                <Label for="requirements">Requirements</Label>
                <Input 
                  type="textarea"
                  name="requirements"
                  id="requirements"
                  placeholder="Requirements"
                  onChange={this.onChange}
                  style={{ height: '200px' }}
                  value={this.state.requirements}
                />
              </FormGroup>
              <FormGroup>
                <Label for="perks">Perks</Label>
                <Input 
                  type="textarea"
                  name="perks"
                  id="perks"
                  placeholder="Perks"
                  onChange={this.onChange}
                  style={{ height: '200px' }}
                  value={this.state.perks}
                />
              </FormGroup>
              <FormGroup>
                <Label for="apply">How to Apply</Label>
                <Input 
                  type="textarea"
                  name="apply"
                  id="apply"
                  placeholder="How to Apply"
                  onChange={this.onChange}
                  style={{ height: '100px' }}
                  value={this.state.apply}
                  required
                />
              </FormGroup>
              <Button
                color="warning"
                style={{ marginTop: '2rem' }}
                block
              >Save</Button>
            </Form>
          </ModalBody>
        </Modal>
      </span>
    )
  }
}

const mapStateToProps = state => ({
  item: state.item
});

export default connect(mapStateToProps, { editItem })(EditModal);