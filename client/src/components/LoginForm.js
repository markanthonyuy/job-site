import React, { Component } from 'react';
import {
  Container,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Alert
} from 'reactstrap';

import JobList from './JobList';
import AddModal from './AddModal';

class LoginForm extends Component {
  state = {
    username: '',
    password: '',
    isLoggedIn: false,
    invalidUser: false
  }

  onChange(e) {
    this.setState({ [e.target.id]: e.target.value });
  }

  authenticate(e) {
    e.preventDefault();
    if(this.state.username === 'admin' && this.state.password === 'admin8') {
      this.setState({ isLoggedIn: true });
    } else {
      this.setState({ invalidUser: true });
    }
  }

  render() {
    return(
      this.state.isLoggedIn ? (
        <div>
          <Container>
            <AddModal />
          </Container>
          <JobList />
        </div>
      ) : <Container>
        <Alert color="danger" isOpen={this.state.invalidUser} fade={true}>
          Invalid User Credentials!
        </Alert> 
        <Form onSubmit={this.authenticate.bind(this)}>
          <FormGroup>
            <Label for="username">Username</Label>
            <Input 
              type="input"
              id="username"
              placeholder="Username"
              onChange={this.onChange.bind(this)}
              required
            />
          </FormGroup>
          <FormGroup>
            <Label for="password">Password</Label>
            <Input 
              type="password"
              id="password"
              placeholder="Password"
              onChange={this.onChange.bind(this)}
              required
            />
          </FormGroup>
          <Button color="dark" block>Login</Button>
        </Form>
      </Container>
    )
  }
}

export default LoginForm;