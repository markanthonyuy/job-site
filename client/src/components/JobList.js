import React, { Component } from 'react';
import { Container, ListGroup, ListGroupItem } from 'reactstrap';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { connect } from 'react-redux';
import { getItems } from '../actions/jobActions';
import DeleteModal from './DeleteModal';
import EditModal from './EditModal';
import ViewModal from './ViewModal';
import TimeAgo from 'react-timeago';
import PropTypes from 'prop-types';

class JobList extends Component {

  componentDidMount() {
    this.props.getItems();
  }

  render() {
    const { items } = this.props.item;

    return(
      <Container>
        <ListGroup>
          <TransitionGroup className="shopping-list">
            {items.map(({ _id, name, type, company, description, requirements, perks, apply, date }) => (
              <CSSTransition key={_id} timeout={500} classNames="fade">
                <ListGroupItem className="clearfix">
                  {name} @ {company} - <i style={{'fontSize': '12px'}}>{type} | <TimeAgo date={date} /></i> 
                  <DeleteModal 
                    id={_id} 
                    name={name}
                    type={type}
                    company={company}
                    date={date} />
                  <EditModal 
                    id={_id} 
                    name={name} 
                    type={type} 
                    company={company} 
                    description={description}
                    requirements={requirements} 
                    perks={perks} 
                    apply={apply} 
                    />
                  <ViewModal 
                    name={name} 
                    type={type} 
                    company={company} 
                    description={description}
                    requirements={requirements}
                    perks={perks} 
                    apply={apply} 
                    date={date} />
                </ListGroupItem>
              </CSSTransition>
            ))}
          </TransitionGroup>
        </ListGroup>
      </Container>
    );
  }
}

JobList.propTypes = {
  getItems: PropTypes.func.isRequired,
  item: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  item: state.item
});

export default connect(mapStateToProps, { getItems })(JobList);