const express = require('express');
const router = express.Router();

// Item Model
const Item = require('../../models/Item');

// @route GET api/items
// @desc  Get all Items
// @access Public
router.get('/', (req, res) => {
  Item.find()
    .sort({ date: -1 })
    .then(items => res.json(items));
});

// @route POST api/items
// @desc  Create a Item
// @access Public
router.post('/', (req, res) => {
  const newItem = new Item({
    name: req.body.name,
    description: req.body.description,
    type: req.body.type,
    company: req.body.company,
    requirements: req.body.requirements,
    perks: req.body.perks,
    apply: req.body.apply,
  });

  newItem.save().then(item => {
    res.json(item);
  });
});

// @route PUT api/items
// @desc  Edit a Item
// @access Public
router.put('/:id', (req, res) => {
  Item.findById(req.params.id)
    .then(job => {
      job.set({
        name: req.body.name,
        description: req.body.description,
        type: req.body.type,
        company: req.body.company,
        requirements: req.body.requirements,
        perks: req.body.perks,
        apply: req.body.apply,
      }).save(() => {
        res.json(job);
      });
    })
    .catch(err => res.status(404).json({ success: false }));
  
});

// @route DELETE api/items/:id
// @desc  Delete a Item
// @access Public
router.delete('/:id', (req, res) => {
  Item.findById(req.params.id)
    .then(item => item.remove().then(() => res.json({ success: true })))
    .catch(err => res.status(404).json({ success: false }));
});

module.exports = router;